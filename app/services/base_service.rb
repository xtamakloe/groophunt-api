class BaseService < Aldous::Service
   def service_failure(params={})
    Result::Failure.new(params)
  end


  def service_success(params={})
    Result::Success.new(params)
  end

   def default_result_data
     {message:nil}
   end
end