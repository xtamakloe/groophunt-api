class CreateGroupService < BaseService

  def initialize(group_data)
    @member_id         = group_data[:member_id]
    @group_name        = group_data[:name]
    @group_description = group_data[:description]
    @image_attachment  = group_data[:pic]
  end


  def perform
    # Find member
    member = Member.where(id: member_id).first
    return service_failure(message: 'Member not found') unless member

    # Create group
    group = Group.new(name: group_name, description: group_description)
    if self.image_attachment
      group.pic = ActionDispatch::Http::UploadedFile.new(self.image_attachment)
    end

    if group.save
      # Create membership
      membership = Membership.new(member_id: member_id,
                                  group_id:  group.id,
                                  admin:     true)
      # TODO: implement CreateMembershipService.perform(member_id, group_id, true)
      if membership.save
        service_success(group: group, membership: membership)
      else
        group.destroy # Remove group

        service_failure(message: membership.errors.full_messages)
      end
    else
      service_failure(message: group.errors.full_messages)
    end
  end


  def default_result_data
    # {group:nil, message:nil}
    super.merge({ group: nil })
  end


  protected

  attr_reader :member_id,
              :group_name,
              :group_description,
              :image_attachment
end