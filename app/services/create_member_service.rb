class CreateMemberService < BaseService

  def initialize(member_signup_data)
    @phone_no = member_signup_data[:phone_no]
    @signup_code = member_signup_data[:signup_code]
  end


  def perform
    code = SignupCode.where(code: self.signup_code).first
    return service_failure(message: 'Invalid Signup Code') unless code

    # Valid request if saved phone no is the same as the one from the current
    # request
    unless code.phone_no == self.phone_no
      return service_failure(message: 'Incorrect Signup Code')
    end

    member = Member.new(username: code.username, phone_no: self.phone_no)
    if member.save
      code.destroy # Clear code

      service_success(member: member)
    else
      service_failure(message: member.errors.full_messages)
    end
  end


  def default_result_data
    # {member: nil, message: nil}
    super.merge({message: nil})
  end

  protected

  attr_accessor :signup_code, :phone_no
end