class ManageGroupMembershipService < BaseService

  def initialize(action, membership_data)
    @action = action
    @group_id = membership_data[:group_id]
    @member_id = membership_data[:member_id]
  end


  def perform
    # Find member
    member = Member.where(id: member_id).first
    return service_failure(message: 'Member not found') unless member

    group = Group.where(id: group_id).first
    return service_failure(message: 'Group not found') unless group

    # Add member to group
    if create_membership?
      membership = Membership.new(member_id: self.member_id, group_id: self.group_id)
      if membership.save
        return service_success(membership: membership)
      else
        return service_failure(message: membership.errors.full_messages)
      end
    end

    # Remove member from group
    if cancel_membership?
      membership = Membership.where(member_id: self.member_id, group_id: self.group_id).first
      if membership
        membership.destroy
        return service_success
      else
        return service_failure(message: 'Member not found')
      end
    end
  end


  def default_result_data
    # {membership:nil, message:nil}
    super.merge({membership: nil})
  end


  def create_membership?
    self.action == :join
  end


  def cancel_membership?
    self.action == :cancel
  end


  protected

  attr_reader :member_id, :group_id, :action
end
