class RequestSignupCodeService < BaseService
  def initialize(request_params)
    @phone_no = request_params[:phone_no]
    @username = request_params[:username]
  end


  def perform
    # See if user has already requested a code
    # TODO: check only valid codes
    code = SignupCode.where(phone_no: phone_no).first

    # Create new code
    code = SignupCode.create(username: username, phone_no: phone_no) unless code

    if code
      # Queue sending code
      SendSmsJob.perform_async(code.phone_no, code.decorate.message)

      # Return api success
      service_success(signup_code: code)
    else
      # Return api failure
      service_failure(message: code.errors.full_messages)
    end
  end

  def default_result_data
    # {signup_code: nil, message: nil}
    super.merge({signup_code: nil})
  end

  protected

  attr_reader :phone_no, :username
end
