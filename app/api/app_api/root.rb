module AppAPI
  class Root < Grape::API
    include AppAPI::Concern::Exceptionable
    include AppAPI::Concern::APIRespondable

    prefix 'api'

    format :json
    default_format :json
    formatter :json, Grape::Formatter::ActiveModelSerializers
    error_formatter :json, AppAPI::ErrorFormatter

    mount AppAPI::V1::Root
  end
end