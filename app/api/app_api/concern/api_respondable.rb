# Responses for all api versions
module AppAPI
  module Concern
    module APIRespondable
      extend ActiveSupport::Concern

      included do
        helpers do
          def api_success(data={})
            {status: 'success', data: data}
          end


          def api_fail(code, message, http_status_code=200)
            error!({code: code, message: message}, http_status_code)
          end
        end
      end

    end
  end
end
