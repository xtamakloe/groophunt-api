module AppAPI
  module ErrorFormatter
    def self.call(message, backtrace, options, env)
      error = message.is_a?(Hash) ? message : { message: message }

      { status: 'error', data: {error: error} }.to_json
    end
  end
end