module AppAPI
  module V1
    class Memberships < Grape::API

      resource :memberships do
        # curl -v -d '{"member_id": "7", "group_id":"1"}' -X POST -H Content-Type:application/json http://localhost:3000/api/v1/memberships
        desc 'Create a new group membership'
        params do
          requires :group_id, type: Integer, desc: 'ID of group'
          requires :member_id, type: Integer, desc: 'ID of member'
        end
        post do
          result = ManageGroupMembershipService.perform(
                  :join,
                  {
                     member_id: params[:member_id],
                     group_id: params[:group_id]
                   })
          if result.success?
            api_success
          else
            api_fail('', message: result.message)
          end
        end


        desc 'Cancel a group membership'
        params do
          requires :group_id, type: Integer, desc: 'ID of group'
          requires :member_id, type: Integer, desc: 'ID of member'
        end
        delete do
          result = ManageGroupMembershipService.perform(
                    :cancel,
                    {
                      member_id: params[:member_id],
                      group_id: params[:group_id]
                    })
          if result.success?
            api_success
          else
            api_fail('', message: result.message)
          end
        end
      end

    end
  end
end
