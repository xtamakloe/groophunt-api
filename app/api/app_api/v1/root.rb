module AppAPI
  module V1
    class Root < Grape::API
      version 'v1'

      mount AppAPI::V1::Groups
      mount AppAPI::V1::Members
      mount AppAPI::V1::Memberships
      mount AppAPI::V1::SignupCodes
    end
  end
end