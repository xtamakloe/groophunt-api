module AppAPI
  module V1
    class SignupCodes < Grape::API

      resource :signup_codes do
        # curl -v -d '{"username": "kojo1", "phone_no":"233244817759"}' -X POST -H Content-Type:application/json http://localhost:3000/api/v1/signup_codes
        desc 'Request a new signup code for registration'
        params do
          requires :username, type: String, desc: 'Member username'
          requires :phone_no, type: String, desc: 'Phone number of member'
        end
        post do

          reg_info = {username: params[:username], phone_no: params[:phone_no]}
          result = RequestSignupCodeService.perform(reg_info)
          if result.success?
            api_success
          else
            api_fail('', message: result.message)
          end
        end
      end

    end
  end
end
