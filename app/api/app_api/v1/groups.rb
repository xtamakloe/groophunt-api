module AppAPI
  module V1
    class Groups < Grape::API
      include Grape::Kaminari

      resource :groups do
        desc 'Create a new group'
        params do
          requires :member_id, type: Integer, desc: 'ID of member creating group'
          requires :group_name, type: String, desc: 'Name of group'
          optional :group_description, type: String, desc: 'Description of group'
          optional :pic, type: Rack::Multipart::UploadedFile, desc: 'Group picture'
        end
        post do
          image_upload =
            if params[:pic]
              {
                filename: params[:pic][:filename],
                type:     params[:pic][:type],
                headers:  params[:pic][:head],
                tempfile: params[:pic][:tempfile]
              }
            else
              nil
            end

          result = CreateGroupService.perform({
                      member_id:   params[:member_id],
                      name:        params[:group_name],
                      description: params[:group_description],
                      pic:         image_upload
                    })

          if result.success?
            api_success(GroupSerializer.new(result.group))
          else
            api_fail(1, result.message)
            # error!({code: code, message: message}, http_status_code)
          end
        end


        desc 'Retrieve a list of all groups'
        paginate per_page: 25
        get do
          groups = Group.order('created_at desc')

          api_success(groups: ActiveModel::ArraySerializer.new(paginate(groups)))
        end


        desc 'Retrieve details of a group'
        params do
          requires :id, type: Integer, desc: 'Group ID'
        end
        route_param :id do
          get do
            group = Group.where(id: params[:id]).first
            if group
              api_success(GroupSerializer.new(group))
            else
              api_fail('', 'Group not found', 404)
            end
          end
        end


        desc 'Update details of a group'
        params do
          requires :id, type: Integer, desc: 'Group ID'
          requires :member_id, type: Integer, desc: 'ID of member performing update'
          optional :name, type: String, desc: 'New name for group'
          optional :uid, type: String, desc: 'New UID for group'
          optional :description, type: String, desc: 'New description for group'
        end
        route_param :id do
          put do
            update_attrs               = {}
            update_attrs[:uid]         = params[:uid] if params[:uid].presence
            update_attrs[:name]        = params[:name] if params[:name].presence
            update_attrs[:description] = params[:description] if params[:description].presence

            # Check if group exists
            group                      = Group.where(id: params[:id]).first
            unless group
              return api_fail(100, 'Group not found', 404)
            end

            # Check if request is from admin
            admin_member = group.memberships
              .where(admin: true, member_id: params[:member_id])
              .first
            return api_fail(100, 'Not authorized to update group') unless admin_member

            if group.update_attributes(update_attrs)
              api_success(GroupSerializer.new(group))
            else
              api_fail(100, group.errors.full_messages)
            end
          end
        end

        # TODO:
        # - destroy a group
        # - find group by id (so admins can )
        # - rate group?
      end

    end
  end
end