module AppAPI
  module V1
    class Members < Grape::API

      resource :members do
        # curl -v -d '{"signup_code": "1211242", "phone_no":"233244817759"}' -X POST -H Content-Type:application/json http://localhost:3000/api/v1/members
        desc 'Create a new member'
        params do
          requires :phone_no, type: String, desc: 'Phone number of member'
          requires :signup_code, type: String, desc: 'Signup code provided by member'
        end
        post do
          result = CreateMemberService.perform({
                      signup_code: params[:signup_code],
                      phone_no: params[:phone_no]
                    })
          if result.success?
            api_success(MemberSerializer.new(result.member))
          else
            api_fail(100, message: result.message)
          end
        end


        # TODO:
        # update member details
        # destroy member
      end

    end
  end
end
