class GroupSerializer < ActiveModel::Serializer
  attributes :id,
             :uid,
             :name,
             :description,
             :pic_url,
             :members
  #TODO: add admin?


  def members
    ActiveModel::ArraySerializer.new(member_list)
  end


  def pic_url
    self.object.pic.url(:medium)
  end

  private

  def member_list
    @member_list ||= object.members
  end
end
