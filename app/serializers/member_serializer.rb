class MemberSerializer < ActiveModel::Serializer
  attributes :id, :username, :phone_no
end
