class Group < ActiveRecord::Base
  has_many :memberships, dependent: :destroy
  has_many :members, through: :memberships

  validates :name, presence: true

  has_attached_file :pic, styles: { medium: '300x300>', thumb: '100x100>' },
                    default_url:  '/images/placeholder.png'
  validates_attachment_content_type :pic, content_type: /\Aimage\/.*\Z/


  # TODO:
  # group needs a rating
  # group needs an id to be quickly found with
end
