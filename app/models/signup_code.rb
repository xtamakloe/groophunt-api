# TODO: implement removal of expired codes, consider redis instead
class SignupCode < ActiveRecord::Base
  before_create :generate_code
  before_create :set_expiry_date

  validates :username, :phone_no, presence: true
  validates :phone_no, uniqueness: true


  protected

  # Generate random 7 digit number
  def generate_code
    self.code = loop do
      random_code = Random.new.rand(1_000_000..10_000_000-1)
      break random_code unless SignupCode.exists?(code: random_code)
    end
  end


  def set_expiry_date
    self.expires_at = DateTime.now + 5.minutes
  end
end
