class Member < ActiveRecord::Base
  validates :username, presence: true
  validates :phone_no, presence: true

  has_many :memberships, dependent: :destroy
  has_many :groups, through: :memberships
end
