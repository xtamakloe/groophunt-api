class CreateSignupCodes < ActiveRecord::Migration
  def change
    create_table :signup_codes do |t|
      t.string :username
      t.string :phone_no
      t.string :code
      t.datetime :expires_at

      t.timestamps null: false
    end
    add_index :signup_codes, :code
  end
end
