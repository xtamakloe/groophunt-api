class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :username
      t.string :phone_no

      t.timestamps null: false
    end
    add_index :members, :username
    add_index :members, :phone_no
  end
end
