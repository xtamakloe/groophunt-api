class AddPicToGroups < ActiveRecord::Migration
  def up
    add_attachment :groups, :pic
  end


  def down
    add_attachment :groups, :pic
  end
end
