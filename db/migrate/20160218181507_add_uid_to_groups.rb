class AddUidToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :uid, :string
    add_index :groups, :uid
  end
end
